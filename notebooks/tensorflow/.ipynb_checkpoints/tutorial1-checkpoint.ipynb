{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this note book, you will learn the basics of computation with TensorFlow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import convention"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import tensorflow as tf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computation graph"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The backbone of TensorFlow is *computation graph*. All computation is done within them, be it function evaluation, linear regression or neural networks. A computation graph specifies *constants*, *variables*, *placeholder* and the operands that connect them.\n",
    "\n",
    "Computation graphs are *symbolic*, like formulas we write on paper. They don't have any particular value or produce anything until we *evaluate* them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with a simple constant that holds a simple real number:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "const = tf.constant(1.0, name='c')\n",
    "const"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that `const` is *not* a number but a Python object that is describe as:\n",
    "- Being a tensor (which is a generalization of vectors and matrices)\n",
    "- Has a certain name\n",
    "- Has a certain shape (in this case it's scalar so `shape` is empty)\n",
    "- Has a certain data type\n",
    "\n",
    "To get the value of this minimalist graph, you can evaluate it using this conventional code snippet:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "with tf.Session() as session:\n",
    "    print session.run(const)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Constants can also hold any NumPy vectors or matrices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "const = tf.constant([1, 2, 3], name='c')\n",
    "const"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "const = tf.constant(np.random.rand(10, 3), name='c')\n",
    "const"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Throughout this notebook, we shall use a polynomial as a working example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = tf.constant(np.linspace(-2, 2, 20), name='x')\n",
    "y = 0.4*x*x*x + 2*x*x - 1.5*x + 3\n",
    "y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "import matplotlib.pyplot as plt "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "with tf.Session() as session:\n",
    "    x_val, y_val = session.run([x, y])\n",
    "    plt.plot(x_val, y_val)       # line plot\n",
    "    plt.show()                   # <-- shows the plot (not needed with pylab)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Graph with parameters: Using placeholders"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Constants are not so useful because they never change. We can use placeholders which are much similar to function parameters. Each time we evaluate a graph, we can provide a new value to placeholders."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = tf.placeholder(\"float\", (3, 1))\n",
    "with tf.Session() as session:\n",
    "    print session.run(x, feed_dict={x: [[1], [2], [3]]})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can define the same polynomial but this time, different coefficients can be \"fed\" each time we evaluate the graph. Try different values and see how the graph changes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = tf.constant(np.linspace(-2, 2, 20), name='x', dtype=\"float\")\n",
    "coeff = tf.placeholder(\"float\", 4)\n",
    "y = coeff[0]*x*x*x + coeff[1]*x*x + coeff[2]*x + coeff[3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "with tf.Session() as session:\n",
    "    x_val, y_val = session.run([x, y], feed_dict={coeff: [0.4, 2, -1.5, 3]})\n",
    "    plt.plot(x_val, y_val)       # line plot    \n",
    "    plt.show()           # <-- shows the plot (not needed with pylab)     "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Trainable graphs: Using variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The curve we've just created can't be fitted to data because we always need to explicitly specify coefficients. We can make it more useful by using *variables* instead of constants/placeholders.\n",
    "\n",
    "Variables are elements that hold its value accross runs (evaluations) of the graph but can be updated using a special function called *assign*. Using gradient descent, we change variables and therefore fit the curve to data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variables is defined with an initial value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = tf.Variable(x + 5, name='a')\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "coeff = tf.Variable([0.4, 2, -1.5, 3], name='coeff')\n",
    "coeff"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "TensorFlow requires us to always *explicitly* initialize variables. The conventional way is to create an operation and run it at the beginning of each session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "init_op = tf.initialize_all_variables()\n",
    "with tf.Session() as sess:\n",
    "    sess.run(init_op)\n",
    "    print(sess.run(coeff))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume we have 4 datapoints as training data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x_data = [-1.5, -0.5, 0.5, 1.5]\n",
    "y_data = [4, 10, 9, 8.5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The curve as it is now is not a good fit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def plot_curve(sess, x):\n",
    "    x_val = np.linspace(-2, 2, 20)\n",
    "    y_val = sess.run(y, feed_dict={x: x_val})\n",
    "    plt.plot(x_val, y_val)          # line plot\n",
    "    plt.plot(x_data, y_data, 'o')\n",
    "    plt.show()           # <-- shows the plot (not needed with pylab)         \n",
    "\n",
    "x = tf.placeholder(\"float\")\n",
    "y = coeff[0]*x*x*x + coeff[1]*x*x + coeff[2]*x + coeff[3]\n",
    "init_op = tf.initialize_all_variables()\n",
    "with tf.Session() as sess:\n",
    "    sess.run(init_op)\n",
    "    plot_curve(sess, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The magic comes with gradient (more about it later). To define gradients, we need a loss, e.g. sum of squares."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y_gold = tf.placeholder(\"float\")\n",
    "loss = tf.reduce_sum((y_gold-y)*(y_gold-y))\n",
    "d_coeff = tf.gradients(loss, coeff)[0]\n",
    "step = coeff.assign_add(-0.01 * d_coeff)\n",
    "d_coeff"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can fit this curve!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "init_op = tf.initialize_all_variables()\n",
    "with tf.Session() as sess:\n",
    "    sess.run(init_op)\n",
    "    for i in range(20):\n",
    "        c = sess.run(step, feed_dict={x: x_data, y_gold: y_data})\n",
    "    plot_curve(sess, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Auto-differentiation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We saw the use of `tf.gradient` in the last section. In this section we'll look closer and get a feeling of what it does.\n",
    "\n",
    "To compute gradients $\\partial \\ell / \\partial w$, we use the command `tf.gradients(ell, w)`. To calculate the gradients of more than one parameter, we can use: `tf.gradients(ell, [w, b, ...])`\n",
    "\n",
    "To check that the derivation is correct, let's compute $\\frac{d}{dx}(x^2)$ in two ways:\n",
    "\n",
    "1. By hands: $\\frac{d}{dx}(x^2) = 2x$\n",
    "2. By TensorFlow: $\\frac{d}{dx}(x^2) = $ `tf.gradients`($x^2$, x)\n",
    "\n",
    "Notice that `tf.gradients` always returns a list of objects so remember to unpack it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = tf.placeholder('float64')\n",
    "y = x*x\n",
    "dy_dx, = tf.gradients(y, [x])\n",
    "with tf.Session() as sess:\n",
    "    x_val = np.linspace(-2, 2, 20)\n",
    "    dy_dx_val = sess.run(dy_dx, feed_dict={x: x_val}) # compute gradient by TensorFlow\n",
    "    dy_dx_val2 = x_val * 2 # compute gradient by hand\n",
    "    print np.allclose(dy_dx_val, dy_dx_val2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, let's check that `tf.gradients`$(\\log x, x) = \\frac{1}{x}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = tf.placeholder('float64')\n",
    "y = tf.log(x)\n",
    "dy_dx, = tf.gradients(y, [x])\n",
    "with tf.Session() as sess:\n",
    "    x_val = np.linspace(0.5, 2.5, 20)\n",
    "    dy_dx_val = sess.run(dy_dx, feed_dict={x: x_val}) # compute gradient by TensorFlow\n",
    "    dy_dx_val2 = 1.0/x_val # compute gradient by hand\n",
    "    print np.allclose(dy_dx_val, dy_dx_val2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also compute gradients of a vector and gradients of more than one variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "x = tf.placeholder('float64', 10)\n",
    "W = tf.placeholder('float64', 10) # vector of 10 elements\n",
    "b = tf.placeholder('float64', ()) # scalar\n",
    "z = tf.reduce_sum(x*W) + b\n",
    "y = tf.nn.softmax(tf.reshape(z, [1, -1]))\n",
    "ell = tf.reduce_sum(y)\n",
    "tf.gradients(ell, [W, b])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interactive demo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In iPython/Jupyter notebook, we can use interactive sessions to run our graph across notebook cells. However, we'll need to explicitly close the session later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sess = tf.InteractiveSession()\n",
    "# do stuff here\n",
    "sess.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this cell once:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sess = tf.InteractiveSession()\n",
    "init_op = tf.initialize_all_variables()\n",
    "sess.run(init_op)\n",
    "step_count = 0\n",
    "def make_a_step():\n",
    "    global step_count\n",
    "    c = sess.run(step, feed_dict={x: x_data, y_gold: y_data})\n",
    "    step_count += 1\n",
    "    print '#steps executed: %d' %step_count\n",
    "    plot_curve(sess, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this cell repeatedly to observe the curve changes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "make_a_step()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you're done, close the session (the previous cell won't work after that)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sess.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
