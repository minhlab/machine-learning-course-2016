{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will learn necessary tools to build a neural network:\n",
    "\n",
    "* Tensor operations\n",
    "* Aggregate operations\n",
    "* Neural network building blocks\n",
    "* Training routines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tensor operations\n",
    "\n",
    "Similar to NumPy, TensorFlow supports a wide range of operations on tensors. Let's start with the creation of zero matrices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This is a NumPy matrix:\n",
      "[[ 0.  0.  0.]\n",
      " [ 0.  0.  0.]]\n",
      "This is a TensorFlow object:\n",
      "Tensor(\"zeros:0\", shape=(2, 3), dtype=float32)\n",
      "This is its value:\n",
      "[[ 0.  0.  0.]\n",
      " [ 0.  0.  0.]]\n"
     ]
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "sess = tf.InteractiveSession()\n",
    "\n",
    "print \"This is a NumPy matrix:\\n\", np.zeros((2, 3))\n",
    "m = tf.zeros((2, 3))\n",
    "print \"This is a TensorFlow object:\\n\", m\n",
    "print \"This is its value:\\n\", sess.run(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the result of `tf.zeros()` is a TensorFlow object holding a zero matrix but not the matrix itself (different from NumPy). To obtain the matrix, you need to evaluate the TensorFlow object by calling `sess.run()`\n",
    "\n",
    "Similarly, we can create other basic matrices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.ones((2,3))\n",
    "m = tf.ones((2,3))\n",
    "print m\n",
    "print sess.run(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.random.rand(2,3) # draw real numbers uniformly between [0, 1)\n",
    "m = tf.random_uniform((2,3))\n",
    "print m\n",
    "print sess.run(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.random.randint(10, size=(2,3)) # draw integers uniformly between [0, 10)\n",
    "# In TensorFlow, use the same function but specify dtype and maxval to get integers\n",
    "m = tf.random_uniform((2,3), maxval=10, dtype=\"int64\")  \n",
    "print m\n",
    "print sess.run(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Always remember to fix the random seed in your experiments. In NumPy, this is as simple as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"Two NumPy random matrices are identical because the seed was set\"\n",
    "np.random.seed(38409272)\n",
    "print np.random.rand(5)\n",
    "np.random.seed(38409272)\n",
    "print np.random.rand(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In TensorFlow, things get a bit more complicated since there are two levels of random seed: graph level and operation level. Each command is an operation (e.g. `random_uniform()` and `random_normal()` are two different operations) and has a separate random seed. To fix a sequence, you need to specify *both* seed.\n",
    "\n",
    "- Specify graph-level random seed by `tf.set_random_seed()`\n",
    "- Specify operation-level random seed using the paramter `seed=...`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tf.set_random_seed(38409272)\n",
    "print sess.run(tf.random_uniform((5,), seed=0))\n",
    "tf.set_random_seed(38409272)\n",
    "print sess.run(tf.random_uniform((5,), seed=0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, if you set graph-level random seed *plus* reset your Python environment, you also get the same sequence of random numbers. This is not easy to test inside Jupyter notebooks, you can try it in command line. I did and it works!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.linspace(-1, 1, 5)\n",
    "m = tf.linspace(-1.0, 1.0, 5)\n",
    "print m\n",
    "print sess.run(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.arange(10)\n",
    "m = tf.range(10)\n",
    "print m\n",
    "print sess.run(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You might have noticed that the API of TensorFlow is a bit different, usually more concrete and somewhat more cumbersome. If you're already familiar with NumPy, it would take some time to get familiar to but in the end it shouldn't matter much.\n",
    "\n",
    "Let's continue with reading and manipulating the shape of tensors. This is how we do it with NumPy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = np.arange(9)+1\n",
    "print \"We start with a flat vector:\\n\", a\n",
    "print \"Shape:\", a.shape\n",
    "a = a.reshape((3,3))\n",
    "print \"We can make a square matrix out of it:\\n\", a\n",
    "print \"Shape:\", a.shape\n",
    "a = a.ravel()\n",
    "print \"And then flatten it again with np.ravel():\\n\", a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do the same thing with TensorFlow, just that the notation is a bit different:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "m = tf.range(9)+1\n",
    "print \"Start with a flat vector again:\\n\", sess.run(m)\n",
    "print \"Shape:\", m.get_shape()\n",
    "m = tf.reshape(m, (3,3))  # m.reshape wont' work\n",
    "print \"Square matrix, piece of cake:\\n\", sess.run(m)\n",
    "print \"Shape:\", m.get_shape()\n",
    "m = tf.reshape(m, (-1,))  # there's no tf.flatten() or tf.ravel()\n",
    "print \"Flattened using tf.reshape():\\n\", sess.run(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Transposing is possible but doesn't look that nice:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"Transposing a NumPy matrix:\"\n",
    "a = (np.arange(6)+1).reshape((2,3))\n",
    "print a\n",
    "print a.T\n",
    "print \"Transposing a TensorFlow matrix:\"\n",
    "m = tf.reshape(tf.range(6)+1,(2,3))\n",
    "print sess.run(m)\n",
    "print sess.run(tf.transpose(m))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A certain type of indexing (i.e. selecting embeddings from an embeddings matrix given word indices) is done with `tf.gather`. Right now, TensorFlow doesn't support the full range of indexing functionalities of NumPy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "E = tf.random_uniform((10,3))\n",
    "x = [0, 2, 3, 0, 2]\n",
    "sess.run(tf.gather(E, x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aggregate operations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For our purposes, the most important aggreate operations are summing and averaging."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"=== NumPy ===\"\n",
    "v = np.random.rand(10, 3)\n",
    "print \"Sum of everything:\", v.sum()\n",
    "print \"Sum of each rows:\", v.sum(axis=1)\n",
    "print \"Sum of each rows, keeping dimensions:\\n\", v.sum(axis=1, keepdims=True)\n",
    "print \"=== TensorFlow ===\"\n",
    "m = tf.constant(v)\n",
    "print \"Sum of everything:\", sess.run(tf.reduce_sum(m))\n",
    "print \"Sum of each rows:\", sess.run(tf.reduce_sum(m, reduction_indices=(1,)))\n",
    "print \"Sum of each rows, keeping dimensions:\\n\", \\\n",
    "        sess.run(tf.reduce_sum(m, reduction_indices=(1,), keep_dims=True))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"=== NumPy ===\"\n",
    "v = np.random.rand(10, 3)\n",
    "print \"Mean of everything:\", v.mean()\n",
    "print \"Mean of each rows:\", v.mean(axis=1)\n",
    "print \"Mean of each rows, keeping dimensions:\\n\", v.mean(axis=1, keepdims=True)\n",
    "print \"=== TensorFlow ===\"\n",
    "m = tf.constant(v)\n",
    "print \"Mean of everything:\", sess.run(tf.reduce_mean(m))\n",
    "print \"Mean of each rows:\", sess.run(tf.reduce_mean(m, reduction_indices=(1,)))\n",
    "print \"Mean of each rows, keeping dimensions:\\n\", \\\n",
    "        sess.run(tf.reduce_mean(m, reduction_indices=(1,), keep_dims=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building blocks for neural networks\n",
    "\n",
    "Recall that an artificial neuron works by first computing the net input and then passing it through an activation function. The net input is simply a linear combination of inputs, weighted by the neuron's own weights, plus a bias. The operation is also call dot product and is usually denoted by a \"middle dot\":\n",
    "\n",
    "$$z = x \\cdot W + b$$\n",
    "\n",
    "In NumPy, we use `np.dot` to compute the dot product:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "W = np.random.rand(10, 3)  # 10 hidden units, each accepting 3 inputs\n",
    "b = np.random.rand(10)     # 10 bias for 10 hidden units\n",
    "x = np.random.rand(100, 3) # a batch of 100 examples, each is represented by 3 numbers\n",
    "z = np.dot(x, W.T) + b\n",
    "z[:2] # net input for the first two examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In TensorFlow, `np.dot()` is replace by `tf.matmul()` (matrix multiplication):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "W = tf.random_uniform((10, 3))  # 10 hidden units, each accepting 3 inputs\n",
    "b = tf.zeros((10,))     # 10 bias for 10 hidden units\n",
    "x = tf.random_uniform((100, 3)) # a batch of 100 examples, each is represented by 3 numbers\n",
    "z = tf.matmul(x, tf.transpose(W)) + b\n",
    "sess.run(z[:2]) # net input for the first two examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can compute activations using different functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = tf.sigmoid(z) # sigmoid function\n",
    "sess.run(a[:2]) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = tf.tanh(z) # hyperbolic tangent\n",
    "sess.run(a[:2]) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = z*z*z # cubic function\n",
    "sess.run(a[:2]) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = tf.nn.relu(z) # rectified linear\n",
    "sess.run(a[:2]) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can compute the output layer with softmax, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "W2 = tf.random_uniform((5, 10))  # 5 output units, each accepting 10 inputs (from hidden layer)\n",
    "z2 = tf.matmul(a, tf.transpose(W2))\n",
    "y = tf.nn.softmax(z2)\n",
    "sess.run(y[:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes it's more convenient and efficient to compute the log of softmax instead of softmax itself (e.g. to use in the negative log likelihood log)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y = tf.nn.log_softmax(z2)\n",
    "sess.run(y[:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sigmoid is good if there is only one output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "W2 = tf.random_uniform((1,10))\n",
    "z2 = tf.matmul(a, tf.transpose(W2))\n",
    "y = tf.reshape(tf.nn.sigmoid(z2), (-1,))\n",
    "sess.run(y[:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute the loss, it is common to use negative log likelihood. Given a set of gold labels, it is computed as:\n",
    "\n",
    "$$ \\ell = - \\frac{1}{m} \\sum_{i=1}^m \\log p(y_\\mathrm{gold}(i)) $$\n",
    "\n",
    "In TensorFlow, this is doable with a rather lengthy function: `tf.nn.sparse_softmax_cross_entropy_with_logits`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "W2 = tf.random_uniform((5, 10))  # 5 output units, each accepting 10 inputs (from hidden layer)\n",
    "z2 = tf.matmul(a, tf.transpose(W2))\n",
    "y_gold = tf.random_uniform((100,), maxval=5, dtype=\"int64\")\n",
    "loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(z2, y_gold))\n",
    "sess.run(loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the function operates on *net input* of the output layer instead of the activation (softmax). It is designed so for efficiency. To obtain the output of softmax (probability distribution) anyway, you need to evaluate it along side the loss."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "y = tf.nn.softmax(z2)\n",
    "sess.run((y[:2], loss))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also show the predicted labels using `argmax`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "labels = tf.argmax(y, 1)\n",
    "sess.run((labels[:2], loss))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training your neural network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This part demonstrates how you can train a neural network more conveniently than what we did before. But until now we created everything as constants for convenience but they are not trainable. So first, we need to wrap `tf.Variable()` around all the weights and biases and re-create our model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "W = tf.Variable(tf.random_uniform((10, 3)))  # 10 hidden units, each accepting 3 inputs\n",
    "b = tf.Variable(tf.zeros((10,)))     # 10 bias for 10 hidden units\n",
    "x = tf.random_uniform((100, 3)) # a batch of 100 examples, each is represented by 3 numbers\n",
    "z = tf.matmul(x, tf.transpose(W)) + b\n",
    "a = z*z*z # cubic function\n",
    "W2 = tf.Variable(tf.random_uniform((5, 10)))  # 5 output units, each accepting 10 inputs (from hidden layer)\n",
    "z2 = tf.matmul(a, tf.transpose(W2))\n",
    "y_gold = tf.random_uniform((100,), maxval=5, dtype=\"int64\")\n",
    "loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(z2, y_gold))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the help of functions from `tf.train` package, training is now simpler. Training with simple gradient descent means creating an operation of the type `tf.train.GradientDescentOptimizer` and calling it. Bare in mind that it does essentially the same thing that we did last session, i.e. compute the gradient and update the parameters according to the rule $\\theta := \\theta - \\alpha \\Delta \\theta$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "loss = 1.77315\n",
      "loss = 1.72722\n",
      "loss = 1.75965\n",
      "loss = 1.67054\n",
      "loss = 1.63851\n",
      "loss = 1.60835\n",
      "loss = 1.65264\n",
      "loss = 1.60078\n",
      "loss = 1.6351\n",
      "loss = 1.63599\n"
     ]
    }
   ],
   "source": [
    "opt = tf.train.GradientDescentOptimizer(learning_rate=0.1)\n",
    "opt_op = opt.minimize(loss)\n",
    "init_op = tf.initialize_all_variables()\n",
    "sess.run(init_op)\n",
    "for i in range(10):\n",
    "    print \"loss =\", sess.run((loss, opt_op))[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more challenging update rules, it's better to just use TensorFlow's operations instead of implementing by ourselves."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "loss = 1.8554\n",
      "loss = 1.63603\n",
      "loss = 1.65214\n",
      "loss = 1.64931\n",
      "loss = 1.68058\n",
      "loss = 1.65382\n",
      "loss = 1.63731\n",
      "loss = 1.59419\n",
      "loss = 1.64011\n",
      "loss = 1.62689\n"
     ]
    }
   ],
   "source": [
    "opt = tf.train.AdagradOptimizer(learning_rate=0.1)\n",
    "opt_op = opt.minimize(loss)\n",
    "init_op = tf.initialize_all_variables()\n",
    "sess.run(init_op)\n",
    "for i in range(10):\n",
    "    print \"loss =\", sess.run((loss, opt_op))[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "loss = 2.87765\n",
      "loss = 2.10579\n",
      "loss = 1.78303\n",
      "loss = 1.70917\n",
      "loss = 1.63331\n",
      "loss = 1.61876\n",
      "loss = 1.57622\n",
      "loss = 1.61257\n",
      "loss = 1.61114\n",
      "loss = 1.63802\n"
     ]
    }
   ],
   "source": [
    "opt = tf.train.AdamOptimizer(learning_rate=0.1)\n",
    "opt_op = opt.minimize(loss)\n",
    "init_op = tf.initialize_all_variables()\n",
    "sess.run(init_op)\n",
    "for i in range(10):\n",
    "    print \"loss =\", sess.run((loss, opt_op))[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
