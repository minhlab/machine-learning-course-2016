COL_WORD = 0
COL_POS = 1
COL_HEAD = 2
COL_LABEL = 3

class ArcStandard(object):
    def __init__(self, sentence):
        self.sentence = sentence
        self.reset()

    def reset(self):
        self.links = list([w, None, -1, None] for w in self.sentence)
        self.stack = [-1]
        self.buffer = range(len(self.sentence))

    def execute(self, action, label=None):
        if action == 'L':
            assert len(self.stack) >= 2, 'Too few elements in stack'
            s1, s2 = self.stack[0], self.stack[1]
            self.links[s2][COL_HEAD] = s1
            self.links[s2][COL_LABEL] = label
            del self.stack[1]
        elif action == 'R': 
            assert len(self.stack) >= 2, 'Too few elements in stack'
            s1, s2 = self.stack[0], self.stack[1]
            self.links[s1][COL_HEAD] = s2
            self.links[s1][COL_LABEL] = label
            del self.stack[0]
        elif action == 'S':
            assert(len(self.buffer) >= 1)
            self.stack.insert(0, self.buffer[0])
            del self.buffer[0]
        else:
            raise ValueError('Unsupported action: %s' %action)

    def oracle(self, gold_links):
        actions = []
        self.reset()
        a = self.oracle_next(gold_links)
        while a:
            actions.append(a)
            self.execute(*a)            
            a = self.oracle_next(gold_links)
        return actions

    def get_right_dependent(self, gold_links, i):
        for j in reversed(range(i+1, len(gold_links))):
            if gold_links[j][COL_HEAD] == i: return j

    def check_right_depency(self, gold_links, s1):
        if self.get_right_dependent(gold_links, s1) is None: return True
        if self.get_right_dependent(gold_links, s1) == self.get_right_dependent(self.links, s1): return True
        return False

    def oracle_next(self, gold_links):
        '''
            based on Malt parser's implementation:
            http://grepcode.com/file/repo1.maven.org/maven2/org.maltparser/maltparser/1.8/org/maltparser/parser/algorithm/nivre/ArcStandardOracle.java#ArcStandardOracle
        '''
        debug = False
        buffer = self.buffer
        stack = self.stack
        if len(stack) < 2:
            if len(buffer) <= 0:
                if debug: print stack, self.links
                assert stack[0] == -1
                return None
            else:
                if debug: print('shift')
                return 'S', None
        else:
            s1, s2 = stack[0], stack[1]
            if s2 >= 0 and gold_links[s2][COL_HEAD] == s1:
                if debug: print('left') 
                return 'L', gold_links[s2][COL_LABEL]
            elif (s1 >= 0 and gold_links[s1][COL_HEAD] == s2 and 
                    self.check_right_depency(gold_links, s1)):
                if debug: print('right') 
                return 'R', gold_links[s1][COL_LABEL]
            elif len(buffer) >= 1:
                if debug: print('shift') 
                return 'S', None
            else:
                raise ValueError("You shouldn't see this! Probably unprojective tree was used.")

class SwapStandard(object):
    def __init__(self, sentence):
        self.sentence = sentence
        self.reset()

    def reset(self):
        self.links = list([w, None, -1, None] for w in self.sentence)
        self.stack = [-1]
        self.buffer = range(len(self.sentence))

    def execute(self, action, label=None):
        if action == 'Sw':
            assert len(self.stack) >= 2, 'Too few elements in stack'
            self.stack[0], self.stack[1] = self.stack[1], self.stack[0]
        elif action == 'L':
            assert len(self.stack) >= 2, 'Too few elements in stack'
            s1, s2 = self.stack[0], self.stack[1]
            self.links[s2][COL_HEAD] = s1
            self.links[s2][COL_LABEL] = label
            del self.stack[1]
        elif action == 'R': 
            assert len(self.stack) >= 2, 'Too few elements in stack'
            s1, s2 = self.stack[0], self.stack[1]
            self.links[s1][COL_HEAD] = s2
            self.links[s1][COL_LABEL] = label
            del self.stack[0]
        elif action == 'S':
            assert(len(self.buffer) >= 1)
            self.stack.insert(0, self.buffer[0])
            del self.buffer[0]
        else:
            raise ValueError('Unsupported action: %s' %action)

if __name__ == '__main__':
    sys = ArcStandard('Peter and Mary brought a car .'.split(' '))
    sys.execute('S')
