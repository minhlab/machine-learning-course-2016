from collections import defaultdict
from dep import *
import os
from IPython.display import Image, display
import sys

image_count = 0

def to_tex(tree, path):
    with open(path, 'w') as f:
        f.write(r'''\documentclass[preview]{standalone}
% In the preamble:
\usepackage{xytree}
\usepackage[utf8]{inputenc}
\begin{document}
% In the document:
\xytext{
''')
        children = defaultdict(list)
        for i in range(len(tree)):
            head = tree[i][COL_HEAD]
            children[head].append(i)
        link_level = defaultdict(int)
        for k in range(1, len(tree)):
            for i in range(len(tree)):
                head = tree[i][COL_HEAD]
                if abs(head-i) == k:
                    if head < i:
                        for j in range(head, i):
                            if tree[j][COL_HEAD] >= head:
                                link_level[i] = max(link_level[i], link_level[j])
                    else:
                        for j in range(i+1, head+1):
                            if tree[j][COL_HEAD] >= i:
                                link_level[i] = max(link_level[i], link_level[j])
                    for j in range(len(tree)):
                        if tree[j][COL_HEAD] > min(i, head) and tree[j][COL_HEAD] < max(i, head):
                            link_level[i] = max(link_level[i], link_level[j])
                    link_level[i] += 1
        for i in range(len(tree)):
            f.write('\t\\xybarnode{%s}' %tree[i][COL_WORD])
            if children[i]:
                f.write('\n')
                for c in children[i]:
                    f.write('\t\t\\xybarconnect[%d](' %((link_level[c]-1)*5+3))
                    if c < i: f.write('UL')
                    else: f.write('UR')
                    f.write(',U){%d}"' %((c-i)*2))
                    if c < i: f.write('_{')
                    else: f.write('^{')
                    if tree[c][COL_LABEL]:
                        f.write('\small %s' %tree[c][COL_LABEL])
                    f.write('}"\n')
                f.write('\t\t')
            if i < len(tree)-1:
                f.write(' &~~~&\n')
        f.write('''}
\end{document}''')

def to_image(tree):
    global image_count
    tex_path = 'images/%03d.tex' %image_count
    to_tex(tree, tex_path)
    os.chdir('images')
    tex_path = '%03d.tex' %image_count
    pdf_path = '%03d.pdf' %image_count
    img_path = '%03d.png' %image_count
    assert os.system('pdflatex %s' %tex_path) == 0
    assert os.system('convert -density 300 %s -quality 90 %s' 
                     %(pdf_path, img_path)) == 0
    os.chdir('..')
    img_path = 'images/%03d.png' %image_count
    image_count += 1
    return img_path

def execute_and_display(conf, action, label=None):
    conf.execute(action, label)
    img_path = to_image(conf.links)
    print [conf.sentence[w] if w >= 0 else 'ROOT' 
           for w in reversed(conf.stack)], [conf.sentence[w] for w in conf.buffer]
    display(Image(img_path))

def demo(conf, actions):
    conf.reset()
    for i, a in enumerate(actions):
        if i > 0: print '-'*80
        print 'Transition %d: %s' %(i+1, a)
        sys.stdout.write('Configuration: ')
        if isinstance(a, (list, tuple)):
            action, label = a
        else:
            action, label = a, None
        execute_and_display(conf, action, label)

if __name__ == '__main__':
    tree = [['Peter', 'NNP', 1, 'conj'], ['and', '...', 3, 'subj'],
            ['Mary', 'NNP', 1, 'conj'], ['brought', 'V', -1, 'ROOT'],
            ['a', '...', 5, 'det'], ['car', 'N', 3, 'obj'], 
            ['.', '.', 3, 'punct']]
    to_image(tree)
